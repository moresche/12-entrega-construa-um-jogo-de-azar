const gameOptions = [
    'rock', 
    'paper', 
    'scissors'
]

function getRandomNumber(min, max) {
    min = Math.ceil(min)
    max = Math.floor(max)
    return Math.floor(Math.random() * (max - min)) + min
}

function playerWins(playerChoose, machineChoose)  {
    if (playerChoose === gameOptions[0] && machineChoose === gameOptions[2] 
        || (playerChoose === gameOptions[1] && machineChoose === gameOptions[0]
        || (playerChoose === gameOptions[2] && machineChoose === gameOptions[1]))) {
            return true
    }
}

const handleClick = function(event) {
    const playerChoose = event.target.id
    const machineChoose = gameOptions[getRandomNumber(0, 3)]

    if (playerWins(playerChoose, machineChoose)) {
        alert('Parabéns você venceu!')
    }   else if (playerChoose === machineChoose) {
        alert('Houve um empate, jogue mais uma vez.')
    }   else {
        alert('Que pena, você perdeu! Tente novamente.')
    }
}

const buttons = document.querySelectorAll('#rock, #paper, #scissors')

for (let i = 0; i < buttons.length; i++) {
    buttons[i].addEventListener('click', handleClick)
}
